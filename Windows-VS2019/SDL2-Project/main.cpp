/*
  Updated version of an earlier example, demonstrating good practice. 
*/

//For exit()
#include <stdlib.h>

//For printf()
#include <stdio.h>

//Include SDL library
#include "SDL.h"

//Include SDL_image this library provides
//support for loading different types of images.
#include "SDL_image.h"

const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT= 600;

const int SDL_OK = 0;

int main( int argc, char* args[] )
{
     // Declare window and renderer objects
    SDL_Window*	     gameWindow = nullptr;
    SDL_Renderer*    gameRenderer = nullptr;

    // Temporary surface used while loading the image
    SDL_Surface*     temp = nullptr;

    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture*     backgroundTexture = nullptr;
    SDL_Texture*     playerTexture = nullptr;

    // Player Image source and target. 
    SDL_Rect sourceRectangle;
    SDL_Rect targetRectangle;
    
    // SDL allows us to choose which SDL componets are going to be
    // initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

    if(sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit. 
        printf("Error -  SDL Initialisation Failed\n");
        exit(1);
    }

    gameWindow = SDL_CreateWindow("Hello CIS4008",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags

   
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          printf("Error - SDL could not create renderer\n");
          exit(1);
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        printf("Error - SDL could not create Window\n");
        exit(1);
    }
    
      
    /**********************************
     *    Setup background image     *
     * ********************************/

    // Load the sprite to our temp surface
    temp = IMG_Load("assets/images/background.png");

    if(temp == nullptr)
    {
        printf("Background image not found!");
    }    

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    backgroundTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'image' now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;


    /**********************************
     *    Setup player image     *
     * ********************************/

    // Load the sprite to our temp surface
    temp = IMG_Load("assets/images/dorf2.png");

    if(temp == nullptr)
    {
        printf("Player image not found!");
    }

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    playerTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'temp' now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;

    //Setup source and destination rects
    sourceRectangle.x = 0;
    sourceRectangle.y = 0;

    // Use the texgure info to get the to get the rectangle size.
    // We're using the whole of the sprite image!
    SDL_QueryTexture(playerTexture, 0, 0,
                     &(sourceRectangle.w),    //width
                     &(sourceRectangle.h));   //height

    // I worked this out via trial and error. 
    targetRectangle.x = 100;
    targetRectangle.y = 400;

    // We could choose any scale/size - experiment with this :-D
    targetRectangle.w = sourceRectangle.w * 1.0f;
    targetRectangle.h = sourceRectangle.h * 1.0f;

    //Draw stuff here.

    // 1. Clear the screen
    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)

    SDL_RenderClear(gameRenderer);

    // 2. Draw the scene
    SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);

//  SDL_RenderCopy(gameRenderer, playerTexture, NULL, NULL);
//  SDL_RenderCopy(gameRenderer, playerTexture, NULL, &targetRectangle);
//  SDL_RenderCopy(gameRenderer, playerTexture, &sourceRectangle, NULL);
    SDL_RenderCopy(gameRenderer, playerTexture, &sourceRectangle, &targetRectangle);

    //NOTE:  If the sourceRectangle is NULL - texture is stretched to fill
    //       If the targetRectangle is NULL - whole texture (rather than a region
    //       will be drawn).

    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);

    //Pause to allow the image to be seen
    SDL_Delay( 10000 );

    //Clean up!
    SDL_DestroyTexture(playerTexture);
    playerTexture = nullptr;   
   
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   

    //Shutdown SDL - clear up resorces etc.
    SDL_Quit();

    return 0;
}
